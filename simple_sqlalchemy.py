from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
engine = create_engine('sqlite:///college.db', echo = True)
meta = MetaData()


students = Table(
   'students', meta, 
   Column('id', Integer, primary_key = True), 
   Column('name', String), 
   Column('lastname', String), 
)


def InsertSingleLine():
   ins = students.insert()
   ins = students.insert().values(name = 'Mitch', lastname = 'Fade')
   result = conn.execute(ins)

conn = engine.connect()

def InsertMultiLine():
   conn.execute(students.insert(), [
      {'name':'Rajiv', 'lastname' : 'Khanna'},
      {'name':'Komal','lastname' : 'Bhandari'},
      {'name':'Abdul','lastname' : 'Sattar'},
      {'name':'Priya','lastname' : 'Rajhans'},
   ])


def getResults():
   conn = engine.connect()
   s = students.select().where(students.c.id>8)
   result = conn.execute(s)

   for row in result:
      print (row)

if __name__ == '__main__':
   InsertSingleLine()
   getResults()